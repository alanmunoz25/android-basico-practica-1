import java.util.Arrays;
import java.util.Collections;

//Main.java
public class Main{
  public static void main(String[] args){
    Estudiante[] estudiantes = {
      new Estudiante("Juan", 24),
      new Estudiante("Erica", 20),
      new Estudiante("Emilio", 23),
      new Estudiante("Karina", 21),
      new Estudiante("Eduardo", 24),
      new Estudiante("Tomas", 25)
    };

    System.out.println("Total de estudiantes: " + estudiantes.length);
    System.out.println("<--->");
    Integer[] edad = {5,7,2,9,11,1};
    int sum = 0;

    for (int i=0; i < estudiantes.length; i++) {
      System.out.println(estudiantes[i].getNombre() + ", " + estudiantes[i].getEdad() + " anios");
      edad[i] = estudiantes[i].getEdad();
      sum = sum + edad[i];
    }
    int avg = sum / estudiantes.length;
    int min = Collections.min(Arrays.asList(edad));
    int max = Collections.max(Arrays.asList(edad));
    System.out.println("<--->");
    System.out.println("Edad maxima : " + max);
    System.out.println("Edad minima : " + min);
    System.out.println("Edad promedio :" + avg);
  }

}
